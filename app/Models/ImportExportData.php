<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ImportExportData
 * @package App\Models
 */
class ImportExportData extends Model
{
    use HasFactory;

    /** @var string  */
    protected $table = 'import_export_data';
    /** @var array  */
    protected $guarded = [];
}
