<?php

namespace App\Http\Controllers;

use App\Imports\FoodAndAgriImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function importExportView()
    {
        return view('excel.index');
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function importExcel(Request $request)
    {
        // ToDo: need to add validation for file type...
        Excel::queueImport(new FoodAndAgriImport, $request->import_file, \Maatwebsite\Excel\Excel::XLSX);

        return redirect('importExportView')->with('success', 'All done!');
    }
}
