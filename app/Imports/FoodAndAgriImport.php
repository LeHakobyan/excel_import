<?php

namespace App\Imports;

use App\Models\Category;
use App\Models\Country;
use App\Models\ImportExportData;
use App\Models\Source;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithStartRow;

class FoodAndAgriImport implements ToCollection, WithChunkReading, ShouldQueue, WithStartRow
{
    /**
    * @param Collection $rows
    */
    public function collection(Collection $rows): void
    {
        foreach ($rows as $row) {
            $category = Category::create([
                'name' => $row[9],
                'code' => $row[8]
            ]);

            $country = Country::where('code', '=', $row[3])->first(); // check if record with current 'code' exist
            if (!$country) {
                $country = Country::create([
                    'name' => $row[4],
                    'code' => $row[3],
                    'iso' => $row[5]
                ]);
            }

            $source = Source::where('name', '=', $row[2])->first(); // check if record with current 'name' exist
            if (!$source) {;
                $source = Source::create([
                    'name' => $row[2]
                ]);
            }

            ImportExportData::create([
                'category_id' => $category->id,
                'country_id' => $country->id,
                'source_id' => $source->id,
                'year' => $row[0],
                'type' => $row[1],
                'category_code' => $row[8],
                'category_name' => $row[9],
                'qty_unit' => $row[10],
                'qty' => $row[11],
                'netweight' => $row[12],
                'trade_value' => $row[13],
                'unit_value_netweight' => (float)$row[14],
                'unit_value_qty' => (float)$row[15],
            ]);
        };
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function chunkSize(): int
    {
        return 1000;
    }
}
