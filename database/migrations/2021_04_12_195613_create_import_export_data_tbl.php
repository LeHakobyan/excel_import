<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImportExportDataTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('import_export_data', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->foreignId('category_id')->unsigned()->references('id')->on('categories');
            $table->foreignId('country_id')->unsigned()->references('id')->on('countries');
            $table->foreignId('source_id')->nullable()->unsigned()->references('id')->on('sources');
            $table->year('year');
            $table->enum('type', ['import', 'export']);
            $table->string('category_code', 255);
            $table->string('category_name', 255);
            $table->string('qty_unit', 255)->nullable();
            $table->integer('qty')->unsigned()->nullable();
            $table->integer('netweight')->unsigned()->nullable();
            $table->decimal('trade_value', 20);
            $table->double('unit_value_netweight')->nullable();
            $table->double('unit_value_qty')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('import_export_data');
    }
}
