<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route for view/blade file.
Route::get('importExportView', [\App\Http\Controllers\ExcelController::class, 'importExportView'])->name('importExportView');

// Route for import excel data to database.
Route::post('importExcel', [\App\Http\Controllers\ExcelController::class, 'importExcel'])->name('importExcel');
